# Calendar Parser

## Background and Functionality

The Calendar Parser is a tool to help Support see staff availability in Google Calendar. It combines PTO data from the [Support - Time Off](https://about.gitlab.com/handbook/support/support-time-off.html#one-time-setup-actions) Google Calendar (which is populated by the [PTO by Roots](https://about.gitlab.com/handbook/paid-time-off/#pto-by-roots) app), and [Area of Focus](https://gitlab-com.gitlab.io/support/team/areas-of-focus.html) information from [support-team.yaml](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml). It creates weekday mini-Calendars that groups team members by Region and Area of Focus, and displays overall availability as green-yellow-red.

## Requirements
- Calendar populated with PTO data (PTO by Roots lets you specify an additional calendar to publish PTO data to)
- One or more calendars to publish summary data to
- A spreadsheet where summary data can be written 

### Environmental Variables Expected
The following variables _must_ be set. Ideally they should be set as project variables in GitLab.

#### Data Sources and Permissions
- `CALENDAR_URL` - the URL for the calendar that contains PTO data
- `SERVICE_CREDS` - a `config.json` for a service account that has access to the spreadsheet in `PTO_SPREADSHEET` and each of the calendars above.

#### Team Mapping
- `DEPLOY_TOKEN` - a deploy token that will let you clone the [Support Team Project](https://gitlab.com/gitlab-com/support/team)
- `USERNAME` - a matching username for the `DEPLOY_TOKEN` to clone project

##### Places to write summary data to
- Calendar IDs for each regional calendar
   - `CALENDAR_ID_AMER`
   - `CALENDAR_ID_EMEA`
   - `CALENDAR_ID_APAC`
- `PTO_SPREADSHEET` - Spreadsheet IDs where summary PTO data can be written


## Future
- Currently we delete all events on the regional calendar and rewrite the whole calendar. This is timeconsuming and wasteful.
It would be better to detect the differences and write only days that have changed since the last run.
- The code could be restructured to be more modular. 
- There's no graceful error handling. If you don't have an environmental variable set it will just crash.
