#!/usr/bin/env ruby

require "csv"
require "icalendar"
require "yaml"
require "date"
require 'net/http'
require 'uri'
require "google_drive"
require 'google/apis/calendar_v3'
require 'googleauth'
require 'oauth'
require 'optparse'
require 'optparse/time'

# parse arguments - if -d or --dry-run is present we won't write anywhere
OptionParser.new do |parser|
  parser.on('-d', '--dry-run') do |dryrun|
     DRYRUN = dryrun
  end
end.parse!

session = GoogleDrive::Session.from_service_account_key("config.json")
PTO_SPREADSHEET = ENV['PTO_SPREADSHEET']
ws = session.spreadsheet_by_key(PTO_SPREADSHEET).worksheets[0]

#delete everything but the headers row
if not defined?(DRYRUN)
    ws.delete_rows(2,ws.rows.length-1)
    ws.save
    index = 2
end

CALENDAR_URL = ENV['CALENDAR_URL']
DEPLOY_TOKEN = ENV['DEPLOY_TOKEN'] #token used to clone support-team.yml
output_file = 'support-time-off.csv'
EXCLUDED_FOCUSES = ["Management", "Onboarding", "Operations"]
FOCUS_CAPS = { 'AMER' => 50, 'APAC'=> 1, 'EMEA' => 50}

# pre-script should make sure this file exists
yaml = []
Dir['support-team/data/agents/**/*.yaml'].each do |file|
    yaml.push(YAML.load_file(file))
end

REGIONS = ['AMER', 'APAC', 'EMEA']

REGIONS_WITH_FOCUS = {} # a hash of focuses by region
REGIONS.each do |region|
    if not REGIONS_WITH_FOCUS.has_key?(region) 
        #initialize empty hash if region hasn't been looked at
        REGIONS_WITH_FOCUS[region] = {}
        REGIONS_WITH_FOCUS[region]['ALL'] = 0 
    end
    yaml.select{|x| x['region'].include?(region)}.each do |p|
        #increment regional total for each person in the region
        REGIONS_WITH_FOCUS[region]['ALL'] = REGIONS_WITH_FOCUS[region]['ALL'] + 1

        p['focuses'].each do |focus|
            if not REGIONS_WITH_FOCUS[region].has_key?(focus['name'])
                #initialize empty count if focus hasn't been looked at
                REGIONS_WITH_FOCUS[region][focus['name']] = 0 
            end
            
            if focus['percentage'].to_i >= FOCUS_CAPS[region] # a person exists in a focus so long as they have > 50 percent capacity
               REGIONS_WITH_FOCUS[region][focus['name']] = REGIONS_WITH_FOCUS[region][focus['name']] + 1
            end
        end

    end
end


raw_data = []

# Grab events from Support Time Off
uri = URI.parse(CALENDAR_URL)
response = Net::HTTP.get_response(uri)
calendar = Icalendar::Calendar.parse response.body


  calendar.first.events.each do |event|
    organizer = event.organizer.to_s.split("mailto:")[1]
    begin
        engineer = yaml.select{ |x| x['email'] == organizer }.first
        region = engineer['region']
        focuses = []
        #process focuses
        engineer['focuses'].each do |focus|
            if focus['percentage'].to_i >= 50
                focuses << focus['name']
            end
        end

        focus1 = focuses[0] || ""
        focus2 = focuses[1] || ""
       
    rescue NoMethodError
    end 
    #write to Sheet
    if not defined?(DRYRUN)
        ws[index,1] = event.dtstart
        ws[index,2] = event.dtend
        ws[index,3] = region
        ws[index,4] = focus1
        ws[index,5] = focus2
        index = index + 1
    end
    #build array
    begin
    raw_data.append({"start" => event.dtstart, "end" => event.dtend, "region" => region[0..3],"focus1"=> focus1, "focus2"=>focus2})
    rescue NoMethodError
    end

  end; nil

if not defined?(DRYRUN) 
   ws.save
end


# Generate next years capacity keyed by ISO date to generate calendar events
# e.g. capacity["EMEA"]["2020-12-25"]["License and Renewals"] => 15
# e.g. capacity["EMEA"]["2020-12-25"]["ALL"] => 150 - ALL is always present
p "--- Generating capacity data (showing only non-zero days)"
capacity = {}
for region in REGIONS
    capacity[region] = {}
    for i in 0 .. 364
        date = Date.today + i
        # Logic is mirrored in PTO Calendar Spreadsheet
        cap = raw_data.select { |n| date >= n["start"].to_date and date+1 <= n["end"].to_date and n["region"] == region }
        cap_with_focus = {"ALL" => cap.length} 
        cap.each do |e|
            unless e["focus1"] == ""
                unless cap_with_focus.has_key?(e["focus1"])
                    cap_with_focus[e["focus1"]] = 0
                end
                cap_with_focus[e["focus1"]] = cap_with_focus[e["focus1"]] +1
            end
            
            unless e["focus2"] == ""
                unless cap_with_focus.has_key?(e["focus2"])
                    cap_with_focus[e["focus2"]] = 0
                end
                cap_with_focus[e["focus2"]] = cap_with_focus[e["focus2"]] +1
            end

        end
        # p "#{region} #{date}: #{cap}" if cap.length > 0
        capacity[region][date.iso8601] = cap_with_focus 
    end
end


# set up calendars

RED = 11
YELLOW = 5
GREEN = 2
CALENDARS = {}
CALENDARS["AMER"]=ENV["CALENDAR_ID_AMER"]
CALENDARS["EMEA"]=ENV["CALENDAR_ID_EMEA"]
CALENDARS["APAC"]=ENV["CALENDAR_ID_APAC"]

ENV['GOOGLE_APPLICATION_CREDENTIALS'] = 'config.json'

def createServiceObject()
    service = Google::Apis::CalendarV3::CalendarService.new
    scope = ["https://www.googleapis.com/auth/calendar"]
    service.authorization = Google::Auth.get_application_default(scope)
    service.authorization.fetch_access_token!
    return service
end

service = createServiceObject()

## clear calendars after today
if not defined?(DRYRUN)
    p "--- Bulk deleting calendar events"
    for region, calendar_id in CALENDARS
        p "Deleting events from: #{region}"
        
        page_token = nil
        begin
            result = service.list_events(calendar_id, 
                                        time_min: (DateTime.now - 1).rfc3339, 
                                        page_token: page_token)
            result.items.each do |e|
                service.delete_event(calendar_id, e.id)
                sleep 0.1
            end
            if result.next_page_token != page_token
                page_token = result.next_page_token
            else
                page_token = nil
            end
        end while !page_token.nil?
    end
end

focus_info = {
    'Self-Managed' => {:abb => 'SM'},
    'SaaS' => {:abb => 'SaaS'},
    'License and Renewals' => {:abb => 'L&R'},
    'US Federal' => {:abb => 'Fed'}
}

# Use e.g. https://emojipedia.org instead of string interpolation with unicode values...
emoji_color = {
    GREEN => '🟢',
    YELLOW => '🟡',
    RED => '🔴'
}

def percentage_to_color(percentage)
    if percentage <= 15
        GREEN
    elsif percentage >15 and percentage <=20
        YELLOW
    else
        RED
    end
end

p "--- Writing calendar events "
# Loop through capacity and create events by region
for region in REGIONS

    #exclude some focuses
    corrected_total = REGIONS_WITH_FOCUS[region]["ALL"]

    for excluded_focus in EXCLUDED_FOCUSES
       corrected_total = corrected_total.to_i - REGIONS_WITH_FOCUS[region][excluded_focus].to_i
    end

    for date, focii in capacity[region]

        corrected_count = focii["ALL"]
        for excluded_focus in EXCLUDED_FOCUSES
          corrected_count = corrected_count.to_i - focii[excluded_focus].to_i 
        end
        cap = ((corrected_count.to_f / corrected_total)*100).round #calculate percent out
        color = percentage_to_color(cap)
        description = "Total people out: #{focii["ALL"]} / #{REGIONS_WITH_FOCUS[region]["ALL"]}" 

        focus_ooo = {}
        focus_info.each_key do |key|
            focus_ooo[key] = 0
        end

        for focus, count in focii.sort
            unless focus == "ALL"
                description = description + "\n#{focus}: #{count} / #{REGIONS_WITH_FOCUS[region][focus]}"
                focus_ooo[focus] = count
            end
        end

        emoji_summary = "#{region} "
        for focus, count in focus_ooo.sort
            unless (EXCLUDED_FOCUSES.include? focus) or (focus == 'US Federal' and region != 'AMER')
                focus_capacity = ((count.to_f / REGIONS_WITH_FOCUS[region][focus].to_f) * 100).round
                emoji_summary += focus_info.has_key?(focus) ? "#{focus_info[focus][:abb]}" : "#{focus}"
                emoji_summary += "#{emoji_color[percentage_to_color(focus_capacity)]} "
            end
        end

        description = description + "\nAdjusted counts (subtracting Managers, Onboarding, Operations: used in the % OOO calculation)\n" +
        "Adjusted people out: #{corrected_count} / #{corrected_total}"
        event = Google::Apis::CalendarV3::Event.new(
        summary: "#{emoji_summary}",
        description: "#{description}",
        start: Google::Apis::CalendarV3::EventDateTime.new(
            date: date,
            time_zone: 'America/Los_Angeles'
        ),
        end: Google::Apis::CalendarV3::EventDateTime.new(
            date: date, 
            time_zone: 'America/Los_Angeles'
        ),
        color_id: color
        )
        if not defined?(DRYRUN)
            result = service.insert_event(CALENDARS[region], event)
            sleep 0.2
        end
        if cap > 0
            puts "-- #{region} - #{date}: #{cap}%"
            puts "#{description}"
        end
    end
end
